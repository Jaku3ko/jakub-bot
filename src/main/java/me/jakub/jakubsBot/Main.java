package me.jakub.jakubsBot;

import me.jakub.jakubsBot.commands.*;
import me.jakub.jakubsBot.listeners.MemberJoinListener;
import me.jakub.jakubsBot.listeners.PrivateChannelMessageListener;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;

import javax.security.auth.login.LoginException;

/**
 * @author jakuubkoo
 * @since 23/09/2019
 */
public class Main {

    private static JDA jda;

    public static void main(String[] args) throws LoginException {
        //Bot build
        jda = new JDABuilder(AccountType.BOT)
                .setToken("")
                .setActivity(Activity.listening("!help"))
                .build();

        //Register listeners
        jda.addEventListener(new AboutCommand());
        jda.addEventListener(new PrivateChannelMessageListener());
        jda.addEventListener(new MemberJoinListener());
    }

}
