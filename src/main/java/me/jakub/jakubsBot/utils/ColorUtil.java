package me.jakub.jakubsBot.utils;

import java.awt.*;
import java.util.Random;

/**
 * @author jakuubkoo
 * @since 23/09/2019
 */
public class ColorUtil {

    /**
     * Generate Random ColorUtil
     * @return ColorUtil
     */
    public static Color randomColor() {
        Random colorpicker = new Random();
        int red = colorpicker.nextInt(255) + 1;
        int green = colorpicker.nextInt(255) + 1;
        int blue = colorpicker.nextInt(255) + 1;
        return new Color(red, green, blue);
    }

}
