package me.jakub.jakubsBot.listeners;

import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

/**
 * Listener to auto reply in private channels
 *
 * @author jakuubkoo
 * @since 23/09/2019
 */
public class PrivateChannelMessageListener extends ListenerAdapter {

    @Override
    public void onPrivateMessageReceived(@Nonnull PrivateMessageReceivedEvent e) {

        //Check if user is bot for fix auto reply bug
        if (e.getAuthor().isBot()) return;

        //Send private message
        e.getChannel().sendMessage("Hi, I can't communicate in PM yet, but soon I can.").queue();

        //Close channel
        e.getChannel().close().queue();
    }
}
