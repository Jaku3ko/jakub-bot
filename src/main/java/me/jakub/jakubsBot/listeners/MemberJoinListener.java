package me.jakub.jakubsBot.listeners;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

/**
 * This class do Embed message of Member Join
 *
 * @author jakuubkoo
 * @since 24/09/2019
 */
public class MemberJoinListener extends ListenerAdapter {

    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent e) {

        //Check if user is bot
        if (e.getUser().isBot()) return;

        EmbedBuilder eb = new EmbedBuilder();

        eb.setAuthor("Server Info", null, null);

        eb.setTitle("Join");

        eb.setDescription("User " + e.getUser().getName() + " has joined. Welcome!");

        eb.setImage(e.getUser().getAvatarUrl());

        //Welcome text channel
        TextChannel textChannel = e.getGuild().getTextChannelById("626079375736963072");

        //Prevent text channel delete
        assert textChannel != null : "Text Channel is null!";

        //Send embed message of Member Join Info
        textChannel.sendMessage(eb.build()).queue();
    }
}
