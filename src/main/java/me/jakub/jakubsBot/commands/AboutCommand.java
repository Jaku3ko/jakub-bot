package me.jakub.jakubsBot.commands;

import me.jakub.jakubsBot.utils.ColorUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

/**
 * Command to send some information about this bot
 *
 * @author jakuubkoo
 * @since 23/09/2019
 */
public class AboutCommand extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(@Nonnull GuildMessageReceivedEvent e) {
        User user = e.getAuthor();
        String[] args = e.getMessage().getContentRaw().split(" ");
        TextChannel textChannel = e.getChannel();
        EmbedBuilder eb = new EmbedBuilder();

        //Check if user is bot
        if (user.isBot()) return;

        //Check if message is !about command
        if (args[0].equalsIgnoreCase("!about")){

            //Set color on embed
            eb.setColor(ColorUtil.randomColor());

            //Set author
            eb.setAuthor(e.getAuthor().getName(), null, e.getAuthor().getAvatarUrl());

            //Set title
            eb.setTitle("About this Bot");

            //Set description
            eb.setDescription("Hello my name is Bot, my creator name is Jakub, I have only one job, supervise this server, if you have any idea of improvement me, tell me it on !idea <idea>, if you want help, just type to help channel.");

            //Build Embed and send to the text channel
            textChannel.sendMessage(eb.build()).queue();
        }
    }
}
